mysql -u root -p

SHOW DATABASES;

CREATE DATABASE enrollment_db;


CREATE TABLE teachers (
    id INT NOT NULL AUTO_INCREMENT,
    teacher_id INT,
    teacher_name VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE courses (
    id INT NOT NULL AUTO_INCREMENT,
    course_id INT,
    teacher_id INT NOT NULL,
    course_name VARCHAR(255),
    PRIMARY KEY(id),
    FOREIGN KEY (teacher_id) REFERENCES teachers(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE students (
    id INT NOT NULL AUTO_INCREMENT,
    student_id INT,
    student_name VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE student_courses (
    id INT NOT NULL AUTO_INCREMENT,
    course_id INT NOT NULL,
    student_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (course_id) REFERENCES courses(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    FOREIGN KEY (student_id) REFERENCES students(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

